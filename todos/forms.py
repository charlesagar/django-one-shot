from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class TodoListForm(ModelForm):    # Step 1


    class Meta:                 # Step 2
        model = TodoList        # Step 3
        fields = [              # Step 4
            "name",              # Step 4
        ]


class TodoItemForm(ModelForm):    # Step 1


    class Meta:                 # Step 2
        model = TodoItem        # Step 3
        fields = [              # Step 4
            "task",
            "due_date",
            "is_completed",
            "list",             # Step 4
        ]
