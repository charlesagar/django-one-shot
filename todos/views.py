from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def show_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id) ##looking for a specific to do list object.
    tasks = TodoItem.objects.filter(list=todo_list)
    context = {
        "todo_object": todo_list,
        "tasks": tasks
    }
    return render(request, "details.html", context)

def todo_list_list(request):
    todo_lists = TodoList.objects.all()

    context = {
        'todo_lists': todo_lists,
        }
    return render(request, 'todo_list.html', context)

def create_todo_list(request):
    if request.method == "POST":
        #We should use the form to validate the values
        #   and save them to the database
        form = TodoListForm(request.POST)
        if form.is_valid():
            todos = form.save()
            return redirect("todo_list_detail", id = todos.id)
            # If all goes well, we can redirect the browser
            #   to another page and leave the function

    else:
        #create an instance of Django model form class
        form = TodoListForm()
 #Put the form in the context
    context = {
        "form": form,
    }
    #render the HTML template with the form
    return render(request, "create.html", context)

def edit_todo_list(request, id):

    # Retrieve the existing recipe based on the recipe_id

    if request.method == "POST":
        # Create a form instance with the existing recipe data
        form = TodoListForm(request.POST)
        if form.is_valid():
            todos = form.save()
            # If all goes well, redirect to the recipe details page
            return redirect("todo_list_detail", id= todos.id)
    else:
         # Create a form instance with the existing recipe data
        form = TodoListForm()

    #Put the form in the context
    context = {
        "form": form
         # Include the recipe object in the context
    }

     # Render the HTML template with the form
    return render(request, "edit.html", context)


def delete_todo_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    # Retrieve the existing recipe based on the recipe_id

    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    return render(request, "delete.html")

def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_task = form.save()
            return redirect("todo_list_detail", id=todo_task.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "create_todoitem.html", context)

def edit_todo_item(request, id):
    todo_item = TodoItem.objects.get(id=id)
    # Retrieve the existing recipe based on the recipe_id

    if request.method == "POST":
        # Create a form instance with the existing recipe data
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_task = form.save()
            # If all goes well, redirect to the recipe details page
            return redirect("todo_list_detail", id= todo_task.list.id)
    else:
         # Create a form instance with the existing recipe data
        form = TodoItemForm(instance=todo_item)

    #Put the form in the context
    context = {
        "form": form
         # Include the recipe object in the context
    }

     # Render the HTML template with the form
    return render(request, "edit_todoitem.html", context)
